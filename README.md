# mAssets Price Diffence Chrome Extension

Chrome extension for display the price differences of the oracle and the terraswap on Mirror.finance

<img src="https://i.imgur.com/R61erp9.png"  width="500">

## Installation
**Google Chrome**
1. Download this repo as a [ZIP file](https://gitlab.com/ice5050/mirror.finance-massets-price-differences-chrome-extension/-/archive/0.3.2/mirror.finance-massets-price-differences-chrome-extension-0.3.2.zip).
1. Unzip the file and you should get a folder.
1. In Chrome, go to the extensions page (`chrome://extensions`).
1. Enable Developer Mode.
1. Drag the folder anywhere on the page to import it (do not delete the folder afterwards, otherwise the extension will be gone).

## Build
`yarn run build`
