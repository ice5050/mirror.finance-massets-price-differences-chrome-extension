(() => {
  const handler = details => {
    const {tabId} = details;

    if (tabId) {
      chrome.tabs.sendMessage(tabId, { type: 'page-rendered' });
    }
  };

  const urlFilter = {
    url: [{
      urlEquals: 'https://terra.mirror.finance/'
    }]
  };

  chrome.webNavigation.onCompleted.addListener(handler, urlFilter);
  chrome.webNavigation.onHistoryStateUpdated.addListener(handler, urlFilter);
})();
