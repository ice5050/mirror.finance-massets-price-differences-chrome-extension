const $ = require('jquery');

(() => {
  chrome.runtime.onMessage.addListener(request => {
    if (request && request.type === 'page-rendered') {
      run();
    }
  });

  function run() {
    const interval = setInterval(() => {
      if ($('[class^="Card_main"]').length) {
        try {
          const $table = $('[class^="Card_main"]').last();
          const $rows = $table.find('tr:gt(0)');

          $.each($rows, (_, row) => {
            // Hacky way to fix the duplicated price diff display when spamming clicking Mirror logo
            if ($(row).find('td').get(6).innerText.split('%').length > 1) {
              return;
            }

            // Ignore the MIR coin row (There is no oracle price)
            $name = $(row).find('td').get(1).innerText;
            if ($name === 'MIR') {
              return;
            }

            $oraclePrice    = parseFloat($(row).find('td').get(5).innerText.split(' UST')[0].replace(',', ''));
            $terraswapPrice = parseFloat($(row).find('td').get(6).innerText.split(' UST')[0].replace(',', ''));

            const priceDiff = ($terraswapPrice - $oraclePrice) * 100 / $oraclePrice;

            $($(row).find('td').get(6)).text($(row).find('td').get(6).innerText + ` (${priceDiff.toFixed(2)}%)`);
          });
        } catch (_) {}

        clearInterval(interval);
      }
    }, 1000);
  }
})();
